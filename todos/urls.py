from django.urls import path
from todos.views import show_todos_list, todo_details, create_todo_list

urlpatterns = [
    path("", show_todos_list, name="show_todos_list"),
    path("<int:id>/", todo_details, name="todo_list_detail"),
    path("create/", create_todo_list, name="create_todo_list"),
]
