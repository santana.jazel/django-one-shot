from django.shortcuts import render, redirect, get_object_or_404
from todos.models import TodoList, TodoItem

from todos.forms import TodoListForm


# Create your views here.


def show_todos_list(request):
    todos_list = TodoList.objects.all()

    context = {
        "todos_list": todos_list,
    }

    return render(request, "todos/todos_list.html", context)


def todo_details(request, id):
    todos_list = get_object_or_404(TodoList, id=id)
    context = {
        "todos_object": todos_list,
    }

    return render(request, "todos/detail.html", context)


def create_todo_list(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoListForm()

    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)
